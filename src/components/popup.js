import React from "react";
import Popup from "reactjs-popup";
import ToFilterButton from './buttons/ToFilterButton';
import RandomSpinButton from './buttons/RandomSpinButton';

 const HomePopup = () => (
    <Popup trigger={<button className = 'button'> spin now</button>} position="right center">
      <div>
          <center>
            <p>answer some simple questions</p>
            <ToFilterButton/>
            <p> or </p>
            <RandomSpinButton/>
          </center>
        </div>
    </Popup>
  );

export default HomePopup;