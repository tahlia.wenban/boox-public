import React , { useEffect, useState } from 'react';
import axios from 'axios'
import { URL } from '../config'
import design from '../Pics/design.png'
import SpinAgain from './buttons/SpinAgain'
import AddToBookshelf from './buttons/AddToBookshelf'
import Popup from "reactjs-popup";

const Spin = (props) => {
   const [displaySpinner,setDisplay] = useState(true)
   const [book, setBook] = useState({})
   console.log(props)
  useEffect(() => {
    getBook()
  },[]) 
  useEffect(() => {
    setTimeout(()=> {
      setDisplay(false) 
    }, 2000)
  }, [book])

  const getBook = async() => {
    try {
      const result =  props.location.state == null 
       ? await axios.post(`${URL}/books/random`)
       : await axios.post(`${URL}/books/filter`, props.location.state)
          // const findTitle = result.data.temp.title
          // const findCover = result.data.temp.book_cover
          // const findBook = result.data.temp
          return result.data.ok 
          ? (setBook({...result.data.temp}))
          : null
    }
    catch (error){
      console.log(error)
    }
  }

  const Modal = () => (
   
  <Popup className='popup' trigger={<button className='button right'>add to bookshelf</button>} 
  modal>
  {close => (
    <div className="modal">
      <a className="close" onClick={close}>
        &times;
      </a>
      <div className="header"> what next? </div>
      <div className="actions">
        <SpinAgain/>
        <p className='paragraph'>or</p>
        <AddToBookshelf {...props} 
                        user = {props.user} 
                        updateCollection = {props.updateCollection}
                        book = {book}/>
      </div>
    </div>
  )}
</Popup>
);



  return(
    <div className='spinnerPage'>
      {/* // first routes to filter page/ then to the spinner  
      // when click spin, setTimeout and the route to book page */}
      {displaySpinner 
            ? <div className='spinner'></div> 
            : <div className = 'bookContainer'> 
                <SpinAgain id='hello'/> 
                <img className= 'bookCovers' 
                     src= {book.book_cover}/>
                <Modal/>
            </div>
      }
      <p>{props.user}</p>{console.log('book=====>', book)}
    </div>
  )
}

export default Spin
