import React , { useState } from 'react'
import filter_fields from './filter_fields'
import SpinNowButton from '../buttons/SpinNowButton';
import RandomSpinButton from '../buttons/RandomSpinButton'
import {URL} from '../../config'

//===============================================================
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import axios from 'axios';


const GetFilter = (props) => {
    //console.log(props)
  const [form,setForm] = useState(filter_fields)
  const [step,setStep] = useState(1)
  const [disp_gen_btn, set_disp_gen_btn] = useState(false)
  const [disp_page_btn, set_disp_page_btn] = useState(false)
  const [disp_age_btn, set_disp_age_btn] = useState(false)

  const handleFormChange = ele =>  e => {
      //console.log(e.target.name, e.target.value,ele)
      setForm({...form,[e.target.name]:e.target.value})
      //console.log (e.target.value)
      ele=='page' 
      ? set_disp_page_btn(e.target.value.length > 0 ? true : false) 
      : set_disp_age_btn(e.target.value !== '-' ? true : false)

  }

  const handleGenreCheck = (idx,element) => event => {
      //console.log(event.target.value, event.target.name)
      const temp = element === 'genres' ? form.genres : null
      temp[idx].checked = !temp[idx].checked
      const filtered = temp.filter((ele) => ele.checked )
      setForm({ ...form,[element] : temp});
      set_disp_gen_btn(filtered.length > 0 ? true : false)
  };



//   const handleSpinClick = async (e) => {
//       //e.preventDefault();
//       //const genres = form.genres.filter( ele => ele.checked )
//       const data = {
//         genres : checked('genres'),
//         pages: Number(form.pages),
//         age: form.age
//       }
//       console.log('data =>',data)
//       try {
//         const response = await axios.post(`${URL}/filter_questions/create`, data)
//       }
//       catch {

//       }
//       setForm(filter_fields)
//       //console.log(form);
//   }

  const render_question = () => {
    return <div>
                <div>
                    <center>
                        <p>answer some simple questions</p>
                        <button className='button' onClick={()=>setStep(2)}>tailored spin</button>
                        <p> or </p>
                        <RandomSpinButton/>
                    </center>
                </div>
          </div>
}

  const render_genre = () => {
      return <div>
                <label> <div className='title'>What's your favourite genre?</div>
                {
                    form.genres.map((ele, idx) => {
                        return  <FormControlLabel
                                    control={<Checkbox color="default" checked={ele.checked} 
                                                       onChange={handleGenreCheck(idx,'genres')}/>}
                                    label={ele.name} 
                                />
                    })

                }
                </label>
                <div> 
                    {disp_gen_btn ? <button className='button' onClick={()=>setStep(3)}>proceed</button>  : null}
                </div>
            </div>
  }

  const render_pages = () => {
      return  <div>
                
                <div className='title'><label>What's your page limit?</label></div>
                <input 
                    className='inputBox'
                    placeholder = 'aprox. num'
                    name='pages'
                    value={form.pages}
                    onChange={handleFormChange('page')}/>
                    
              <div><button className='button' onClick={()=>setStep(2)}>go back</button>
              {disp_page_btn ? <button className='button' onClick={()=>setStep(4)}>proceed</button>  : null}
               </div>
            </div>
  }

  const render_age = () => {
      return  <div>
                <label> <div className='title'>What's your age?</div>

                    <select name='age' 
                            onChange={handleFormChange('age')}>
                        <option value="-">-</option>
                        <option value="baby">Baby</option>
                        <option value="young_child">Young Child</option>
                        <option value="preteen">Preteen</option>
                        <option value="teen">Teen</option>
                        <option value="young_adult">Young Adult</option>
                        <option value="adult">Adult</option>
                    </select>
                </label>
                <div><button className='button' onClick={()=>setStep(3)}>go back</button></div>
                {disp_age_btn ? <SpinNowButton {...props} form = {form}/> : null}
            </div>
  }
  let show
  if(step === 1)show = render_question()
  if(step === 2)show = render_genre()
  if(step === 3)show = render_pages()
  if(step === 4)show = render_age()
  

  return(
    <div className = 'App'>
          <div>
                {show} 
          </div>   
    </div>
  )
}

export default GetFilter