import React from 'react';
import { NavLink } from "react-router-dom";
import logo from '../Pics/yellowLogo.png'

const Navbar = () => {
    return (
        <div className = 'nav'>

            <NavLink className = 'navText'
                exact
                to={"/"}
            >
               <img className = 'logo' src={logo}/>
            </NavLink>
            <div></div>
            <div></div>
            <div></div>
            <NavLink  className = 'navText'
                exact
                to={"/filter" } 
            >
                spin 
            </NavLink>
            {/* Takes to questions */}
            <NavLink className = 'navText'
                exact
                to={"/login"}
            >
                sign in
            </NavLink>
            {/* If logged in takes to your account, else takes to login/register page */}
        </div>
    )
}


export default Navbar;
