import React from 'react';
import ToFilterButton from './buttons/ToFilterButton'
// import ToStatsButton from './buttons/ToStatsButton'
// import DeleteButton from './buttons/DeleteButton'
import Axios from 'axios'
import { URL } from '../config'

const Bookshelf = (props) => {
  const books = props.userInfo.books
  const get_book_covers = () => {
    // if(props.userInfo.books.length !== 0){
      console.log('"""""""""""""""',books)
      books.map((book) => {
      return <img className='galleryBook' src={book.book_cover} />
      })
  }

  // console.log('books =======>', books)

  //   const handleImgClick = async () => {
  //     try{
  //       const response = await Axios.post(`${URL}/users/`,{ })
  //       console.log(response.data.ok)
  //     }
  //     catch(error){
  //       console.log(error)
  //     } 
  //   }
  return(
    <div className ='bookshelf'>
      <h1 className ='bigTitle'>{`${props.userInfo.username}'s`}</h1>
      <h2 className = 'bookshelfTitle'>bookshelf</h2>
      <div className = 'collection'>
        {props.userInfo.books 
          ?  props.userInfo.books.length==0
             ? <h2 className='title2'>no books added</h2>
             : (<div className='masonry'>{
                   props.userInfo.books.map((ele,i)=>{
                   return <img className='galleryBook' src={ele.book_cover} />
                   })

             }</div>)
          : null
        }
      </div>
  
      {/* <DeleteButton {...props} 
      user = {props.user} 
      book = {props.book}
      deleteFromCollection = {props.deleteFromCollection}/> */}
      <ToFilterButton className = 'button'/>
      {/* <ToStatsButton className = 'button'/> */}
   </div>
  )
}

export default Bookshelf
