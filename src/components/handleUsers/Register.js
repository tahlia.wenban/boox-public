import React , { useState } from 'react'
import Axios from 'axios' 
import { NavLink } from 'react-router-dom/cjs/react-router-dom';
import PasswordMask from 'react-password-mask';
import { withRouter } from 'react-router-dom';
import { URL } from '../../config'
 

const Register = (props) => {
	const [ form , setValues ] = useState({
		email    : '',
		username: '',
		password : '',
		password2: ''
	})
	const [ message , setMessage ] = useState('')
	const handleChange = e => {
       setValues({...form,[e.target.name]:e.target.value})
	}
	const handleSubmit = async (e) => {
		e.preventDefault()
		try{
			const response =  await Axios.post(`${URL}/users/register`,{
				email    : form.email,
				username: form.username,
			    password : form.password,
				password2: form.password2
			})
	        setMessage(response.data.message)
			//console.log(response)
			console.log(form.username)  
		}
		catch( error ){
			console.log(error)
		}

	}

	const handleClick = e =>  window.location.href='/login'


	return <div><form onSubmit={handleSubmit}
	             onChange={handleChange}
	             className='form_container'>
	         <div><label>Email</label></div>
		     <div><input name="email"/></div>
			 <div><label>Username</label></div>
		     <div><input name="username"/></div>
		     <div><label>Password</label></div>
			 <PasswordMask
			 	className='enterPassword'
				name="password"
				value={form.password}
				onChange={handleChange.bind()}
				useVendorStyles={false}
				buttonClassName = 'showBtn'
			 />
		     <div><label>Repeat password</label></div>
		     <PasswordMask
			 	className='enterPassword'
				name="password2"
				value={form.password2}
				onChange={handleChange.bind()}
				useVendorStyles={false}
				buttonClassName = 'showBtn'
			 />
			 <div><button  onClick = {handleClick} className='button'>register</button></div>
		     <div className='message'><h4>{message}</h4></div>
	       </form>
		   <p>Already have an account?</p>
            <NavLink className = 'navText'
                    exact
                    to={"/login"}
                >
                    <button className = 'button'>log in now</button>
            </NavLink>
            </div>
}

export default withRouter (Register);