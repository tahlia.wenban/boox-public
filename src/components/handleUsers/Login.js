import React , { useState } from 'react';
import Axios from 'axios'; 
import { NavLink } from 'react-router-dom/cjs/react-router-dom';
import PasswordMask from 'react-password-mask';
import { URL } from '../../config'

const Login = (props) => {
	const [ form , setValues ] = useState({
		email: '',
		password : ''
	})
	const [ message , setMessage ] = useState('')

	const handleChange = e => {
       setValues({...form,[e.target.name]:e.target.value})
	}

	const handleSubmit = async (e) => {
		e.preventDefault()
		try{
          const response = await Axios.post(`${URL}/users/login`,{
			email:form.email,
          	password:form.password
		  })
		  setMessage(response.data.message)
		  console.log(response.data.ok)
          if( response.data.ok ){
              localStorage.setItem('token', JSON.stringify(response.data.token)) 
			  //JSON.parse(localStorage.getItem('token'))
			  //localStorage.removeItem('token') 
			  props.login(response.data.username, response.data.email, response.data.uid, response.data.books)
			  setTimeout( ()=> props.history.push('/account'),10)  
          }
          
		}
        catch(error){
        	console.log(error)
        }
	}
	return <div><form onSubmit={handleSubmit}
	             onChange={handleChange}
			  className='form_container'>
             <div><label>email</label></div>
		     <div><input name="email"/></div>
			 <label>password</label>
			 <PasswordMask
			 	className='enterPassword'
				name="password"
				value={form.password}
				onChange={handleChange.bind()}
				useVendorStyles={false}
				buttonClassName = 'showBtn'
			 />
		     <div><button className='button'>log in</button></div>
		     <div className='message'><h4>{message}</h4></div>
	       </form>
           <p>Don't have an account?</p>
            <NavLink className = 'navText'
                    exact
                    to={"/register"}
                >
                    <button className='button'>register now</button>
            </NavLink>
           </div>
}

export default Login










