import React from 'react'
import { NavLink } from 'react-router-dom'

const UserNavbar = () => (
   <div className='navbar'>
   	  <NavLink
        exact
        style={styles.default}
        activeStyle={styles.active}
        to={"/register"}
	  >
        Register
      </NavLink>

      <NavLink
        exact
        style={styles.default}
        activeStyle={styles.active}
        to={"/login"}
      >
        Login
      </NavLink>

   </div>
)
   
export default UserNavbar

const styles = {
  active: {
    color: "gray"
  },
  default: {
    textDecoration: "none",
    color: "white"
  }
};
