import React, { useReducer } from 'react';
import { NavLink, Redirect } from "react-router-dom";


const DeleteButton = (props) => {
  console.log(props.book)
  const handleClick = () => {
    props.deleteFromCollection(props.book)
  }
  
  return(
   <div> 
     <button onClick={handleClick} className = 'button'>X</button>
   </div>
  )
}

export default DeleteButton
