import React from 'react';
import { NavLink } from "react-router-dom";


const SpinNowButton = (props) => {
  console.log(props)


  const checked = () => {
      const checkedItems = []
      props.form.genres.map((ele) => {
          return ele.checked ? checkedItems.push(ele.name) : null 
      }) 
  return checkedItems
 }


  return(
   <div> 
        <button onClick = {()=> props.history.push({
                                            pathname: '/spin',
                                            state: {
                                              age: props.form.age,
                                              pages: props.form.pages,
                                              genres: checked()
                                            }
        })} className = 'button'>spin now</button>
        <div/>      
   </div>
  )
}

export default SpinNowButton
