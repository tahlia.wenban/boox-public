import React, { useReducer } from 'react';
import { NavLink, Redirect } from "react-router-dom";


const AddToBookshelf = (props) => {
  console.log('book from button =======>', props.book)
  const handleClick = () => {
    props.updateCollection(props.book)
  }
  
  return(
   <div> 
     <button onClick={handleClick} className = 'button'>add to bookshelf</button>
   </div>
  )
}

export default AddToBookshelf
