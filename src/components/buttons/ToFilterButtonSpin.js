import React from 'react';
import { NavLink } from "react-router-dom";


const ToFilterButtonSpin = () => {
  return(
   <div> 
    <NavLink
                exact
                to={"/filter"}
            >
        <div className = 'wrap'>
          <button className = 'button'>spin now</button>
        </div>
    </NavLink>
   </div>
  )
}

export default ToFilterButtonSpin
