import React from 'react';
import { NavLink } from "react-router-dom";


const ToFilterButton = () => {
  return(
   <div> 
    <NavLink
                exact
                to={"/filter"}
            >
        <button className = 'button'>spin now</button>
    </NavLink>
   </div>
  )
}

export default ToFilterButton
