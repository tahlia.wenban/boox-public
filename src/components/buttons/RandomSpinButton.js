import React from 'react';
import { NavLink } from "react-router-dom";


const RandomSpinButton = () => {
  
  return(
   <div> 
    <NavLink
                exact
                to={"/spin"}
            >
        <button className = 'button'>random spin</button>
    </NavLink>
   </div>
  )
}

export default RandomSpinButton
