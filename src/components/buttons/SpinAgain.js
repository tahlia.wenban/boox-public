import React from 'react';
import { NavLink } from "react-router-dom";


const SpinAgain = () => {
  
  return(
   <div> 
    <NavLink
                exact
                to={"/spin"}
            >

     { document.body.onkeyup = function(e){
    if(e.keyCode == 32){
      window.location.reload();
    }
    }}

        <button onClick="window.location.reload();" className = 'button left'>spin again</button>
    </NavLink>
   </div>
  )
}

export default SpinAgain
