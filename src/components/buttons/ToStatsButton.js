import React from 'react';
import { NavLink } from "react-router-dom";


const ToSpinButton = () => {
  return(
   <div> 
    <NavLink
                exact
                to={"/stats"}
            >
        <button className = 'button'>view my stats</button>
    </NavLink>
   </div>
  )
}

export default ToSpinButton
