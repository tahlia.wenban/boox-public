import React , { useState } from 'react'
import {URL} from '../../../config'

//===============================================================
import axios from 'axios';


const DeleteBook = () => {
  const [form, setForm] = useState('')

  const handleFormChange = (e) => {
      setForm(e.target.value)
  }


  const handleSubmit = async (e) => {
      e.preventDefault();
      const data = {_id: form.id}
      console.log(form);
      try {
        const response = await axios.delete(`${URL}/books/delete/${form}`)
      }
      catch {

      }
      setForm('')
  }

  return(
    <div>
       <form onSubmit = {handleSubmit}>
          <div>
            <div>
                <label>id</label>
                <input 
                    required
                    placeholder = 'string, required'
                    name='_id'
                        value={form.id}
                        onChange={handleFormChange}/>
            </div>
          </div>
          <button className='button'>submit</button>
       </form>
    </div>
  )
}

export default DeleteBook