import React from 'react'
import {URL} from '../../../config'

//===============================================================
import axios from 'axios';


const GetAllBooks = () => {

  const handleClick = async (req, res) => {
      try {
        const response = await axios.get(`${URL}/books/get_all_books/`)
        console.log(response)
      }
      catch {

      }
    }

  return(
    <div>
        <button onClick = {handleClick} className='button'>GetAllBooks</button>
    </div>
  )
}
export default GetAllBooks
