import React , { useState } from 'react';
import form_fields from './form_fields';
import DeleteBook from '../delete_book/delete_book';
import GetAllBooks from '../get_all_books/get_all_books';
import {URL} from '../../../config';

//===============================================================
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import axios from 'axios';


const GetBook = () => {
  const [form,setForm] = useState(form_fields)

  const handleFormChange = (e) => {
      console.log(e.target.name, e.target.value)
      setForm({...form,[e.target.name]:e.target.value})
  }

  const handleGenreCheck = (idx,element) => event => {
      //console.log(event.target.value, event.target.name)
      const temp = element === 'genres' ? form.genres : form.format
      temp[idx].checked = !temp[idx].checked
      setForm({ ...form, [element] : temp });
  };

  const checked = (element) => {
      const selected = element === 'genres' ? form.genres : form.format
        const checkedItems = []
        selected.map((ele) => {
            return ele.checked ? checkedItems.push(ele.name) : null 
        }) 
    return checkedItems
   }


  const handleSubmit = async (e) => {
      e.preventDefault();
      //const genres = form.genres.filter( ele => ele.checked )
      const data = {
        title: form.title,
        author: form.author,
        book_cover: form.book_cover,
        publication_year : Number(form.publication_year),
        genres : checked('genres'),
        publisher: form.publisher, 
        format : checked('format'),
        description: form.description,
        prizes: form.prizes,
        reviews : [],
        original_language: form.original_language,
        pages: form.pages,
        country: form.country,
        age: form.age,
        setting: form.setting
      }
      console.log('data =>',data)
      if (data.genres.length==0 || data.format.length == 0 || form.age === '')return alert('please select a genre, format and age') 
      //console.log(form);
      try {
        const response = await axios.post(`${URL}/books/create`,data)
      }
      catch {

      }
      setForm(form_fields)
      //console.log(form);
  }

  return(
    <div>
        <div>ADD A BOOK</div>
       <form onSubmit = {handleSubmit}>
          <div>
            <div>
                <label>title</label>
                <input 
                    required
                    placeholder = 'string, required'
                    name='title'
                        value={form.title}
                        onChange={handleFormChange}/>
            </div>
            <div>
                <label>author</label>
                <input 
                    required
                    placeholder = 'string, required'
                    name='author'
                    value={form.author}
                    onChange={handleFormChange}/>
            </div>
            <div>
                <label>book cover</label>
                <input 
                    required
                    placeholder = 'string, required'
                    name='book_cover'
                    value={form.book_cover}
                    onChange={handleFormChange}/>
            </div>
            <div>
                <label>publication year</label>
                <input 
                    required
                    placeholder = 'num, required'
                    name='publication_year'
                    value={form.publication_year}
                    onChange={handleFormChange}/>
            </div>
            <div>
                <label> <div>genres</div>
                {
                    form.genres.map((ele, idx) => {
                        return  <FormControlLabel
                                    control={<Checkbox checked={ele.checked} onChange={handleGenreCheck(idx,'genres')}/>}
                                    label={ele.name} 
                                />
                    })
                }
                </label>
            </div>
            <div>
                <label>publisher</label>
                <input 
                    required
                    placeholder = 'string, required'
                    name='publisher'
                    value={form.publisher}
                    onChange={handleFormChange}/>
            </div>
            <div>
            <label> <div>format</div>
                {
                    form.format.map((ele, idx) => {
                        return  <FormControlLabel
                                    control={<Checkbox checked={ele.checked} onChange={handleGenreCheck(idx,'format')} value={ele.name} />}
                                    label={ele.name}
                                />
                    })
                }
            </label>

            </div>
            <div>
                <label>description</label>
                <input 
                    required
                    placeholder = 'string, required'
                    name='description'
                    value={form.description}
                    onChange={handleFormChange}/>
            </div>
            <div>
                <label>prizes</label>
                <input 
                    placeholder = 'string, not required'
                    name='prizes'
                    value={form.prizes}
                    onChange={handleFormChange}/>
            </div>
            <div>
                <label>reviews</label>
                <input 
                    placeholder = 'array, not required'
                    name='reviews'
                    value={form.reviews}
                    onChange={handleFormChange}/>
            </div>
            <div>
                <label>original language</label>
                <input 
                    required
                    placeholder = 'string, required'
                    name='original_language'
                    value={form.original_language}
                    onChange={handleFormChange}/>
            </div>
            <div>
                <label>pages</label>
                <input 
                    required
                    placeholder = 'num, required'
                    name='pages'
                    value={form.pages}
                    onChange={handleFormChange}/>
            </div>
            <div>
                <label>country</label>
                <input 
                    required
                    placeholder = 'str, not required'
                    name='country'
                    value={form.country}
                    onChange={handleFormChange}/>
            </div>
            <div>
                <label>age
                
                    <select name='age' onChange={handleFormChange} placeholder='none' required>
                        <option value="-">-</option>
                        <option value="baby">Baby</option>
                        <option value="young_child">Young Child</option>
                        <option value="preteen">Preteen</option>
                        <option value="teen">Teen</option>
                        <option value="young_adult">Young Adult</option>
                        <option value="adult">Adult</option>
                    </select>

                </label>
            </div>
            <div>
                <label>setting</label>
                <input 
                    placeholder = 'string, not required'
                    name='setting'
                    value={form.setting}
                    onChange={handleFormChange}/>
            </div>
                    
          </div>
          <button className='button'>submit</button>
       </form>
       <div>DELETcccccc A BOOK</div>
       <DeleteBook/>
       <div>GET ALL BOOKS</div>
       <GetAllBooks/>
    </div>
  )
}

export default GetBook



// title:req.body.title,
// book_cover:req.body.book_cover,
// author:req.body.author,
// publication_year: req.body.publication_year,
// genres: req.body.genres,
// publisher: req.body.publisher, 
// format: req.body.format,
// description: req.body.description,
// prizes: req.body.prizes,
// reviews: req.body.reviews,
// original_language: req.body.original_language,
// pages: req.body. pages,
// country: req.body.country,
// age: req.body.age,
// topics: req.body.topics,
// setting: req.body.setting