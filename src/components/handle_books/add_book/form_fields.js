export default {
    title: '',
    author: '',
    book_cover: '',
    publication_year: '',
    genres: [{checked:false, name:'Action and Adventure'}, {checked:false, name:'Classics'}, {checked:false, name:'Comic and Graphic Novel'}, {checked:false, name:'Crime'}, {checked:false, name:'Drama'}, {checked:false, name:'Fantasy'}, {checked:false, name:'Historical'}, {checked:false, name:'Horror'}, {checked:false, name:'Humor'}, {checked:false, name:'Mystery'}, {checked:false, name:'Romance'}, {checked:false, name:'Sci-Fi'}, {checked:false, name:'Biography/Autobiography'}],
    publisher: '', 
    format: [{checked:false, name:'hardcover'}, {checked:false, name:'paperback'}, {checked:false, name:'electronic'}],
    description: '',
    prizes: '',//[]
    //reviews: [], 
    original_language: '',
    pages: '',
    country: '',
    age: '',
    setting: ''
}

