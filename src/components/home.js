import React, { useState, useEffect } from 'react';
import ToFilterButtonSpin from './buttons/ToFilterButtonSpin';
import { NavLink } from "react-router-dom";
import HomePopup from './popup';
import Cool from '../Pics/cool.png'
import Login from '../Pics/login.svg'
import Spin from '../Pics/spin.svg'
import Love from '../Pics/love.svg'
import Monitor from '../Pics/monitor.svg'
import Boox from '../Pics/booxIllustration.png'
import Arrow from '../Pics/arrow.png'



const Home = () => {

  let [admin, setAdmin] = useState(true)

  const checkAdmin = () => {
    return admin 
    ? <NavLink className = 'navText'
    exact
    to={"/getBook"}
    >
        add book 
    </NavLink>
    : null
  }


  return(
   <div className = 'App'> 
     <div id = 'wallpaper'>
       <div className = 'headerContent'>
          <img className='boox' src = {Boox}/>
          <div className='hometitle'>
            <h3 className='blueText'>Find books matching your personal taste. </h3>
            <p> Tell us what you like, spin the wheel and get awesome book suggestions tailored just for you.</p>
          </div>
          <ToFilterButtonSpin className='button'/>
          <a href='#explain'>
              <img src={Arrow} className ='arrow'/>
          </a>
        </div>
     </div>
     <div className ='homeBody'>
       <div>
         <h1 className = 'title' id='explain' >how it works</h1>
         <div className='howItWorks'> 
            <div className='illustrations'>

            <NavLink className = 'navText'
                exact
                to={"/login"}
            >
                <img className='images' src={Login}/>
            </NavLink>
              <p className='instructionText'>log in</p>
              <p className='subtext'>tell us about <div className='padding'>yourself</div></p>
            </div>
            <div className='illustrations'>

            <NavLink className = 'navText'
                exact
                to={"/filter"}
            >
                <img  className='images' src={Spin}/>
            </NavLink>
              <p className='instructionText'>spin</p>
              <p className='subtext'>get book suggestions <div className='padding'>tailored just for you</div></p>
            </div>
            <div className='illustrations'>

            <NavLink className = 'navText'
                exact
                to={"/login"}
            >
              <img  className='images' src={Love}/>
            </NavLink>
              <p className='instructionText'>add or trash</p>
              <p className='subtext'>spin again or <div className='padding'>add to bookshelf</div></p>
            </div>
            <div className='illustrations'>
            <NavLink className = 'navText'
                exact
                to={"/login"}
            >
              <img  className='images' src={Monitor}/>
            </NavLink>
              <p className='instructionText'>monitor</p>
              <p className='subtext'>visit your <div className='padding'>bookshelf</div></p>
            </div>
         </div>

         <div className='footerDiv'>
            <nav className = 'socialIcons'>
              <i class="fab fa-facebook-f"></i>
              <i class="fab fa-instagram"></i>
            </nav>
          </div>

         {/* Illustrations explaining how it works
          1. login (can click and takes to login site/ questions/ spin)
          2. spin (click takes to question page/ spin / add )
          3. add or trash
          4. monitor (if loged in => can clik and takes to bookcase) */}
         
      </div>
      </div>
   </div>
  )
}

export default Home
