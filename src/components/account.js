import React from 'react';


const Account =  (props) => {
  console.log(props.userInfo)
  return (
    <div>
      <h1>{`Hello ${props.userInfo.username},`}</h1>
      <p>{`email: ${props.userInfo.email}`}</p>
      <p>{`username: ${props.userInfo.username}`}</p>
      <button className = 'button' onClick = {()=>props.logout()}>log out</button>
   </div>
  )
}

export default Account
