import React from 'react';
import { NavLink } from "react-router-dom";
import logo from '../Pics/yellowLogo.png'

const LoggedInNavbar = (props) => {
    return (
        <div className = 'loggedinNav'>

            <NavLink className = 'navText'
                exact
                to={"/"}
            >
               <img className = 'logo' src={logo}/>
            </NavLink>
            <div></div>
            <NavLink className = 'navText'
                exact
                to={"/bookshelf"}
            >
               bookshelf 
            </NavLink>
            {/* <NavLink className = 'navText'
                exact
                to={"/stats"}
            >
               stats 
            </NavLink> */}
            {/* Only show bookshelf and stats if logged in */}
            <NavLink className = 'navText'
                exact
                to={"/filter"}
            >
                spin
            </NavLink>
            {/* Takes to questions */}
            <div></div>
            <NavLink className = 'navText'
                exact
                to={"/account"}
            >
            <p id='username'>{props.userInfo.username}</p>
            </NavLink>
            {/* If logged in takes to your account, else takes to login/register page */}
        </div>
    )
}


export default LoggedInNavbar;
