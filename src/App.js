import React, { useState, useEffect } from 'react';
import Helmet from 'react-helmet'
import Home  from "./components/home";
import Bookshelf  from "./components/bookshelf";
import Stats  from "./components/stats";
import Spin  from "./components/spin";
import Account  from "./components/account";
import GetBook  from "./components/handle_books/add_book/add_book";
import GetFilter  from "./components/filter/filter_questions";

import Login from '../src/components/handleUsers/Login'
import Register from '../src/components/handleUsers/Register'
import UserNavbar from '../src/components/handleUsers/UserNavbar'

import './App.css';
import Axios from 'axios'
import { URL } from './config'

import { BrowserRouter as Router, Route, Redirect } from "react-router-dom";
import LoggedInNavbar from './components/LoggedInNavbar';
import Navbar from './components/Navbar';


function App() {
  let [userInfo, setUserInfo] = useState({})
  let [user, setUser] = useState(false)

  //function that change user content when loggedin and loggetout

  // useEffect when load page check if there is a token in localstorage
  // if token== null setUser to false 
  // : send token to server '/users/verify_token'
  // if ok : true change setUser to true else set to false 

  
  const token = JSON.parse(localStorage.getItem('token'))
	useEffect( () => {
     token === null 
     ? setUser(false)
     : changeContent()
  },[])

//   useEffect( () => {
//     console.log('====>',user)
//  },[user])
  const changeContent = async () => {
		try {
      const response = await Axios.post(`${ URL }/users/verify_token`, {token})
      console.log('response========>', response.data.books)
      return response.data.ok
      ? (
        setUser(true), setUserInfo({
                  ...userInfo,
                  _id:response.data.uid,
                  username:response.data.username, 
                  email:response.data.email,
                  books:response.data.books
              }) 
      ): setUser(false)   
		}
		catch(error) {
			console.log(error)
		}
  }

  const login = (username,email, uid, books) => {
    setUserInfo({
      ...userInfo,
      username: username, 
      email: email,
      _id: uid, 
      books: books
    })
    setUser(true)
  }

  const logout = () => {
    localStorage.removeItem('token');
    setUser(false)
  } 

  const updateCollection = async(book) => {
    try {
      const fetchMyBooks = await Axios.put(`${URL}/users/update_my_books`, {
        _id: userInfo._id,
        book: book
      })
      console.log('added book ========>', fetchMyBooks.data.books)

      setUserInfo({
        ...userInfo,
        books:[...fetchMyBooks.data.books]
      }) 
    }
    catch(error) {
        console.log(error)
    }
  }


  const deleteFromCollection = async(book) => {
    try {
      const fetchMyBooks = await Axios.put(`${URL}/users/delete_my_book`,{
                                                                  _id: userInfo._id,
                                                                  book: book
                                                                })
          console.log('added book ========>', fetchMyBooks.data.books)

          setUserInfo({
            ...userInfo,
            books:fetchMyBooks.data.books
         }) 

          //put the books in the state
    }
    catch(error) {
        console.log(error)
    }
  }



  return (
  <div className = 'App'>
    <Helmet>
      <title>Boox</title>
      <meta charSet="utf-8" />
      <meta name="description" content="Find books matching your personal taste." />
      <meta name='keywords' content='books, book search, find, interests, roulette, wheel, collection'/>
      <meta name='viewport' content='width=device-width, initial-scale=1'/>

      <meta property="og:url" content="https://barcelonacodeschool.com/ux-design-bootcamp-part-time/" />
      <meta property="og:title" content="UX/UI Design part-time Bootcamp in Barcelona Code School" />
      <meta property="og:description" content="UX/UI Design part-time Bootcamp in Barcelona Code School. Part-time 11-week course to become a UX Designer." />
      <meta property="og:image" content="https://barcelonacodeschool.com/static/ux_bootcamp_part_time_barcelona_code_school.a2c9814c.png" />
      <meta property="og:image:url" content="https://barcelonacodeschool.com/static/ux_bootcamp_part_time_barcelona_code_school.a2c9814c.png" />
      <meta name= "twitter:card" content= "summary_large_image"/>
    </Helmet>
    <Router>
          {
            user ? 
            <LoggedInNavbar logout = {logout} userInfo = {userInfo}/>
            : <Navbar/> 
          }
            <Route exact path="/" component={Home}/>
          <div className='body'>
            <Route exact path="/bookshelf" render={ (props)=> {
                                                    return <Bookshelf 
                                                                {...props}
                                                                user = {user} 
                                                                userInfo = {userInfo}
                                                                deleteFromCollection = {deleteFromCollection}
                                                              //pass books lists
                                                                />
                                                    // : <Redirect to = {'/'}/>
                                                  }}/>
            <Route exact path="/stats" render={ ()=> {
                                                    return user
                                                    ? <Stats/>
                                                    : <Redirect to = {'/'}/>
                                                  }}/>
            <Route exact path="/spin" render = {props => <Spin {...props} 
                                                                user = {user} 
                                                                updateCollection = {updateCollection}/>}/>
            <Route exact path="/filter" component={GetFilter}/>
            <Route exact path="/account" render={props => {
                                                    return user
                                                    ? <Account {...props} 
                                                                logout = {logout}
                                                                userInfo = {userInfo}/>
                                                    : <Redirect to = {'/'}/>
                                                  }}/>
            <Route exact path="/getBook" component={GetBook} />

            <Route exact path='/login'  render = {props => { 
                                                    return !user
                                                    ? <Login {...props} login = {login}/>
                                                    : <Redirect to={'/'}/> 
                                                  }}/>
            <Route path='/register'    render = { () => {
                                                    return !user 
                                                    ? <Register />
                                                    : <Redirect to={'/'}/>
                                                }}/>
            </div>
      </Router>
    </div>
  );
}

export default App
